# Unreal Engine Sample Plugin

A sample plugin based on the tutorial presented at 
https://docs.unrealengine.com/en-US/Programming/Rendering/ShaderInPlugin/QuickStart/index.html


Overview
=====
This is basically the exact sample pulled from the above url. Decided to make a repo for it because there are some things in the tutorial that are not mentioned. 

What's been altered
====
* In the tutorial, it does not describe how to reference your shader folder. By default, the shaders will be attempted to be extracted from one of the pre-defined paths in the engine. See `Source/ShaderSample/Private/ShaderSample` to see how the `IPluginManager` is used which is required to add your own paths to the engine for lookup.

* In `Source/ShaderSample.Build.cs`, note that `ShaderCore` is part of `RenderCore` 4.24 and that you will need to add the `Projects` module in order to use `IPluginManager`